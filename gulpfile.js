var gulp = require('gulp');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
gulp.task('watch', function() {
	watch('public/**', function() {
		gulp.start('reload');
	});
});
gulp.task('reload', function() {
	return gulp.src('public/index.html')
		.pipe(connect.reload());
});
gulp.task('connect', function() {
	connect.server({
		root: 'public',
		port: 3334,
		livereload: true
	});
});
gulp.task('default', ['connect','watch']);