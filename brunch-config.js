
var jadeUtils = require('./modules/jadeUtils');
var markdownFrontMatterCompiler = require('./modules/markdown');

var pages = markdownFrontMatterCompiler('app/pages');
var posts = markdownFrontMatterCompiler('app/posts');


module.exports = {
	config: {
		files: {
			javascripts: {
				joinTo: 'app.js'
			},
			stylesheets: {
				joinTo: 'app.css'
			},
			templates: {
				joinTo: 'app.js'
			}
		},
		plugins: {
			brunchTypescript: {
				tscOption: "--module commonJs"
			},
			jade: {
				pretty: true,
				options: {
					pretty: true,
					self: true
				},
				locals: {
					utils: jadeUtils,
					pages: pages,
					posts:posts,
					config: {
						root: '/'
					}
				}
			}
		},
	},
	cache: {
		enabled: false
	}
};