var consola = require('./js/console');
var css = require('./js/css');
var easing = require('./js/easing');
var select = require('./js/select');
var intro = require('./js/intro');

var app = {
	init: function() {

	},
	intro: function() {
		intro.init({
			achicar: 1,
			moveUp: 1
		});
		console.log('intro');
	},
	showContent:function(){
		select('div.content').removeClass('hide');
	},
	page: function() {
		console.log('page');
		css.to('.content').set({
			display: 'block'
		});
	}
};


module.exports = app;