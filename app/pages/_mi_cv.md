---
title: mi cv
description: Bacon is great - let me tell you why I like it.
published: 2014-04-15
author: Javi
categories:
 - food
 - breakfast
 - asd
---

# Javier L. Arancibia       
    -   21-12-90

##   Academic Formation 
    - Software engineer. 
        UBA, Buenos Aires, Argentina. 
    - Technician in Computer Programming. 
     UTN, Mar del Plata, Provincia de Buenos Aires, Argentina.
    - Technician in Computer oriented to economy and organizations.
        E.E.T.Nº 5, Mar del Plata, Provincia de Buenos Aires, Argentina.
    - Programming Camp. E.E.T.Nº 3
        E.E.T.Nº 5, Mar del Plata, Provincia de Buenos Aires, Argentina.
        

##  Special Skills
    - Self taught for excellence.
    - Versatility for handling diferents technologies.
    - Javascript ninja

##  Profesional Formation

### # Clarity S.A   
    - www.clarity.com.ar
    - Senior Web developer
    - six years duration. 2011-2016

### #   Quadramma 
    - www.endigames.com
    - game developer
    - two years 2012/2014

### # Endigames
    - www.quadramma.com
    - Technology Lead developer
    - two years 2012/2014

### # PyM Consulting
    - www.pymconsultores.com
    - assistant teacher
    - six months duration. (2012)

## Technologies
html, angularjs, bootstrap ui, jquery, ajax, http web servicies, wcf, asp, asp .net, mvc5, signalR, javascript, nodejs, websocket, threejs, pixijs, unity3d, unreal engine, java hibernate, ideafix,  cryengine, cocos2dhtml5, coronaSdk, php eden flight medoo cake, mysql, sqlserver, coffescript, wordpress, gulp, mongodb, composer bower npm grunt, mobile phonegap  cocoonjs, flash adobe, air actionscript, lua, python, c++, sap, opengl, sdl, linux, windows, remotedesktop, ssh, stylus, ftp, vpns, git.

## Languages
    - Spanish native
    - English intermediate
    - French basic

## Contact

> arancibiajav@gmail.com

> AR +54 9 11 36789540 

> FR +34 6 72997572