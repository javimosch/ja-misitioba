var consola = require('./console');
var css = require('./css');
var easing = require('./easing');
var select = require('./select');

var intro = {
	init:function(opt){

		opt = opt || {};
		opt.achicar = opt.achicar || 5000;
		opt.moveUp = opt.moveUp || 3000;


		//consola(8);
		console.log('app initialized');
		console.log('app running');
		console.log('app checking');

		achicar(opt);


		var listOfPosts = [];

		window.require.list().filter(function(module) {
			return new RegExp('^posts/').test(module);
		}).forEach(function(module) {
			var post = require(module);
			listOfPosts.push(post);
		});
		console.info(listOfPosts);

		function achicar() {
			var from = select('.logo img').w();
			console.log("from : " + from);
			easing({
				easingFunction: easing.easeInOutQuint,
				from: 200,
				to: 60,
				duration: opt.achicar,
				interval: 30
			}, function(val,porcent) {

				if(porcent>95){
					moveUp(opt);
				}

				css.to('.logo img').set({
					width: parseInt(val) + 'px'
				});
			}, function() {
				
			});
		}


		var _moveUpCalled=false;
		function moveUp() {
			if(_moveUpCalled)return;
			_moveUpCalled = true;
			var height = select('a.logo').pos().y - 5;
			easing({
				easingFunction: easing.easeInOutSine,
				from: 0,
				to: 70,
				duration: opt.moveUp,
				interval: 30
			}, function(val,porcent) {
				//console.log(porcent);
				css.to('a.logo').set({
					position: 'relative',
					top: -val + '%'
				});
			},function(){
				showEmail();
			});

			function showEmail(){
				select('.intro-email').removeClass('hide');
				
				if(select('.console'))
					select('.console').addClass('hide');
			}
		}
	}
};
module.exports =intro;