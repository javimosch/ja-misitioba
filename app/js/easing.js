var easing = (function() {


	var fn = function(s, cb, endCb) {
		var start = new Date().getTime();
		var end = start + s.duration;
		if (s.from > s.to) {
			s.invert = true;
			var aux = s.from;
			s.from = s.to;
			s.to = aux;
			s.target = s.from;
		} else {
			s.invert = false;
			s.target = s.to;
		}
		console.log('easing:' + JSON.stringify(s));

		function loop() {
			setTimeout(function() {
				var now = new Date().getTime();
				var elapsed = now - start;
				s.val = s.easingFunction(elapsed, s.from, s.to, s.duration);
				var porcent = (s.val*s.target)*100;
				if (now >= end) {
					if (s.invert) {
						cb(s.from,porcent);
					} else {
						cb(s.to,porcent);
					}
					if (endCb !== undefined) {
						endCb();
					}
					return;
				} else {
					if (s.invert) {
						s.val = s.to - s.val;
						s.val = (s.val < s.target) ? s.target : s.val;
						cb(s.val,porcent);
					} else {
						s.val = (s.val > s.target) ? s.target : s.val;
						cb(s.val,porcent);
					}
					loop();
				}
			}, s.interval);
		}
		loop(s.from);
	};
	fn.linearTween = function(t, b, c, d) {
		return c * (t / d) + b;
	};
	fn.easeInQuad = function(t, b, c, d) {
		t /= d;
		return c * t * t + b;
	};
	fn.easeInOutSine = function(t, b, c, d) {
		return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
	};
	fn.easeInOutQuint = function(t, b, c, d) {
		t /= d / 2;
		if (t < 1) return c / 2 * t * t * t * t * t + b;
		t -= 2;
		return c / 2 * (t * t * t * t * t + 2) + b;
	};
	fn.easeOutSine = function(t, b, c, d) {
		return c * Math.sin(t / d * (Math.PI / 2)) + b;
	};
	return fn;
})();
module.exports = easing;
/*
easing({
	easingFunction: easing.easeInQuad,
	from: 0,
	to: 10,
	duration: 5000,
	interval: 500
}, function(val) {
	console.info(val);
});
*/