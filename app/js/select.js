var select = function(s) {
	var el = document.querySelector(s);
	if(!el){
		console.warn('select fail '+s);
		return;
	}
	el.css = function(p) {
		var cs = window.getComputedStyle(el);
		return cs[p]
			.replace('px', '');
	};
	el.w = function() {
		return el.css('width');
	};
	el.pos = function() {
		var obj = el;
		var curleft = 0,
			curtop = 0;
		if (obj.offsetParent) {
			do {
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
			return {
				x: curleft,
				y: curtop
			};
		}
		return undefined;
	};

	el.hasClass = function(className) {
		if (this.classList) {
			return this.classList.contains(className);
		} else {
			return (-1 < this.className.indexOf(className));
		}
	};

	el.addClass = function(className) {
		if (this.classList) {
			this.classList.add(className);
		} else if (!this.hasClass(className)) {
			var classes = this.className.split(" ");
			classes.push(className);
			this.className = classes.join(" ");
		}
		return this;
	};

	el.removeClass = function(className) {
		if (this.classList) {
			this.classList.remove(className);
		} else {
			var classes = this.className.split(" ");
			classes.splice(classes.indexOf(className), 1);
			this.className = classes.join(" ");
		}
		return this;
	};

	return el;
};
module.exports = select;