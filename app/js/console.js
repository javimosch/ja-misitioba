var select = require('./select');

function guid() {
    function _p8(s) {
        var p = 'ID'+(Math.random().toString(16)+"000000000").substr(2,2);
        return s ? + p.substr(0,2) +  p.substr(2,2) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

var consola = function(len) {
	var el = document.createElement('div');
	el.setAttribute('class', 'console');
	el.id = guid();
	document.body.appendChild(el);
	var ul = document.createElement('ul');
	el.appendChild(ul);
	
	function write(m,className){
		var li = document.createElement('li');
		li.innerHTML = m;
		if(ul.childNodes.length > len){
			ul.removeChild(ul.childNodes[0]);
		}
		li.id = guid();
		ul.appendChild(li);
		if(className){
			select('#'+li.id).addClass(className);
		}
		el.scrollTop = el.scrollHeight;
	}

	console.log = function(m) {
		write(m,'log');
		console.info(m);
	};


	console.error = function(m){
		write(m,'error');

		if(!select('#'+el.id))
			select('#'+el.id).removeClass('hide');
	};
};

module.exports = consola;