
function getObject(prop,filter){
	var rta = null;
	this[prop].forEach(function(item){
		for(var p in filter){
			if(filter[p] !== item[p]){
				return;
			}
		}
		rta = item;
	});
	return rta;
}

function parsePost(obj){
	var rta = JSON.parse(JSON.stringify(obj));
	return rta;
}

module.exports = function(action,args){
	switch(action){
		case 'page':
			return getObject.apply(this,['pages',args]);
		case 'post':
			return parsePost(getObject.apply(this,['posts',args]));
		default:
			return 'action not found';
	}
};