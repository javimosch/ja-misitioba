var jsYaml = require('yaml-front-matter');
var marked = require('marked');
var hljs = require('highlight.js');
var read = require('read-file');
var fs = require('fs');
function getFiles(dir, files_) {
  files_ = files_ || [];
  var files = fs.readdirSync(dir);
  for (var i in files) {
    var name = dir + '/' + files[i];
    if (fs.statSync(name).isDirectory()) {
      getFiles(name, files_);
    } else {
      files_.push(name);
    }
  }
  return files_;
}

module.exports = function(directory){
  var pageFiles = getFiles(directory);
  var rta = [];
  pageFiles.forEach(function(pageFile) {
    if(pageFile.indexOf('md') === -1) return;
    var data = read.sync(pageFile, 'utf8');
    rta.push(compileMarkdownFrontMatter(data));
  });
  return rta;
};

function compileMarkdownFrontMatter(data) {
  marked.setOptions({
    highlight: function(code, lang) {
      if (lang) {
        return hljs.highlight(lang, code).value;
      } else {
        return hljs.highlightAuto(code).value;
      }
    },
  });
  try {
    var compiled = jsYaml.loadFront(data);
    compiled.__content = marked(compiled.__content);
    return compiled;
  } catch (_error) {
    err = _error;
    return err;
  }
}